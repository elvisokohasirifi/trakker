<!DOCTYPE html>
<html lang="en">
<head>

    <title>Trakker - Expenses</title>

    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="shortcut icon" type="image/png" href="{{ URL::asset('logo.png') }}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('Bootstrap/dist/css/bootstrap-reboot.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('Bootstrap/dist/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('Bootstrap/dist/css/bootstrap-grid.css') }}">
     <link rel="stylesheet" type="text/css" href="{{ URL::asset('print.min.css') }}">

    <!-- Main Styles CSS -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/main.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/fonts.min.css') }}">

    <!-- Main Font -->
    <script src="{{ URL::asset('js/libs/webfontloader.min.js') }}"></script>
    <script>
        WebFont.load({
            google: {
                families: ['Roboto:300,400,500,700:latin']
            }
        });
    </script>

</head>
<body class="body-bg-white">

<!-- Stunning header -->

<div class="stunning-header bg-primary-opacity">

    
    <!-- Header Standard Landing  -->
    
    <div class="header--standard header--standard-landing" id="header--standard">
        <div class="container">
            <div class="header--standard-wrap">
    
                <a href="#" class="logo">
                    <div class="img-wrap">
                        <img src="{{ URL::asset('logo.png') }}" alt="Trakker" style="width: 50px">
                        <img src="{{ URL::asset('logo.png') }}" alt="Trakker" class="logo-colored" style="width: 50px">
                    </div>
                </a>
    
                <a href="#" class="open-responsive-menu js-open-responsive-menu">
                    <svg class="olymp-menu-icon"><use xlink:href="{{ URL::asset('svg-icons/sprites/icons.svg#olymp-menu-icon') }}"></use></svg>
                </a>
    
                <div class="nav nav-pills nav1 header-menu">
                    <div class="mCustomScrollbar">
                        <ul>
                            <li class="nav-item">
                                <a href="/shop/home" class="nav-link">Home</a>
                            </li>

                            <li class="nav-item">
                                <a href="/shop/products" class="nav-link">Products</a>
                            </li>

                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" data-hover="dropdown" data-toggle="dropdown" href="javascript:void(0)" role="button" aria-haspopup="false" aria-expanded="false" tabindex="1">Transactions</a>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="/shop/sales">Sell</a>
                                    <a href="/shop/revenues" class="dropdown-item">Revenues</a>
                                    <a href="/shop/expenses" class="dropdown-item">Expenses</a>
                                </div>
                            </li>

                            <li class="nav-item">
                                <a href="/shop/orders" class="nav-link">Sales</a>
                            </li>

                            <li class="nav-item">
                                <a href="/shop/payments" class="nav-link">Payments</a>
                            </li>

                            @if(auth()->user()->role == 'shopadmin')
                            <li class="nav-item">
                                <a href="/shop/users" class="nav-link">Users</a>
                            </li>
                            @endif

                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" data-hover="dropdown" data-toggle="dropdown" href="javascript:void(0)" role="button" aria-haspopup="false" aria-expanded="false" tabindex="1">Account</a>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="/shop/account">My Account</a>
                                    <a href="/logout" class="dropdown-item">Log Out</a>
                                </div>
                            </li>
                            
                            <li class="close-responsive-menu js-close-responsive-menu">
                                <svg class="olymp-close-icon"><use xlink:href="{{ URL::asset('svg-icons/sprites/icons.svg#olymp-close-icon') }}"></use></svg>
                            </li>
                            <li class="nav-item js-expanded-menu">
                                <a href="#" class="nav-link">
                                    <svg class="olymp-menu-icon"><use xlink:href="{{ URL::asset('svg-icons/sprites/icons.svg#olymp-menu-icon') }}"></use></svg>
                                    <svg class="olymp-close-icon"><use xlink:href="{{ URL::asset('svg-icons/sprites/icons.svg#olymp-close-icon') }}"></use></svg>
                                </a>
                            </li>
    
                            <li class="menu-search-item">
                                <a href="#" class="nav-link" data-toggle="modal" data-target="#main-popup-search">
                                    <svg class="olymp-magnifying-glass-icon"><use xlink:href="{{ URL::asset('svg-icons/sprites/icons.svg#olymp-magnifying-glass-icon') }}"></use></svg>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- ... end Header Standard Landing  -->
    <div class="header-spacer--standard"></div>

    <div class="stunning-header-content">
        <h1 class="stunning-header-title">{{$shop->name}}</h1>
        <ul class="breadcrumbs">
            <li class="breadcrumbs-item">
                <a href="/">Home</a>
                <span class="icon breadcrumbs-custom">/</span>
            </li>
            <li class="breadcrumbs-item active">
                <span>Expenses</span>
            </li>
        </ul>
    </div>

    <div class="content-bg-wrap stunning-header-bg2"></div>
</div>

<section class="medium-padding100">
    <div class="container">
        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        <ul>
             @foreach ($errors->all() as $error)
                 <li class="alert alert-danger">{{ $error }}</li>
             @endforeach
        </ul>

        <?php 
            function timeAgo($timestamp){
                $datetime1=new DateTime("now");
                $datetime2=date_create($timestamp);
                $diff=date_diff($datetime1, $datetime2);
                $timemsg='';
                if($diff->y > 0){
                    $timemsg = $diff->y .' year'.($diff->y > 1 ? "s":'');
                    $timemsg = $timemsg.' ago';
                }

                else if($diff->m > 0){
                 $timemsg = $diff->m .' month'.($diff->m > 1 ? "s":'');
                 $timemsg = $timemsg.' ago';
                }

                else if($diff->d > 0){
                 $timemsg = $diff->d .' day'.($diff->d > 1 ? "s":'');
                 $timemsg = $timemsg.' ago';
                }
                
                else{
                 $timemsg = "today";
                }

                return $timemsg;
            }
         ?>
   
        <div class="row">
            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 m-auto table-responsive">
                <button class="btn btn-primary" data-toggle="modal" data-target="#addmodal">Add New Expense</button>
                <hr>
            </div>
            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 m-auto table-responsive">
                
                <table class="table table-striped table-responsive">
                    <tr class="head">
                        <th>Name</th>
                        <th>Description</th>
                        <th>Payment Nature</th>
                        <th>Amount</th>
                        <th>Date</th>
                        <th>Actions</th>
                    </tr>
                
                    @forelse($expenses as $expense)

                    <tr>
                        <td class="position bold">{{ $expense->name }}</td>
                        <td class="date">{{ $expense->description }}</td>
                        <td class="type">{{ $expense->nature }}</td>
                        <td class="type">{{ $expense->amount }}</td>
                        <td class="position bold"><?php echo timeAgo($expense->created_at); ?></td>
                        <td>
                            <button class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#editmodal" onclick="edititem({{ $expense->id}}, '{{$expense->name }}', '{{$expense->description }}', '{{$expense->nature }}', {{$expense->amount }})">Edit</button>
                            <button class="btn btn-success btn-sm" data-toggle="modal" data-target="#deletemodal" onclick="document.getElementById('did').value = {{ $expense->id }}">Delete</button>
                            
                        </td>                        
                    </tr>

                    @empty
                    <p>No expenses have been made yet</p>
                    @endforelse
            
                </table>

                {{ $expenses->links() }}

            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="addmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Add New Expense</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="/shop/addexpense" method="post" id="addexpenseform">
            @csrf
            <div class="form-group">
                <label for="name">Name / Title</label>
                <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="" name="name" required="required">
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <textarea class="form-control @error('description') is-invalid @enderror" id="description" name="description" value="" required="required"></textarea>
            </div>
            <div class="form-group">
                <label for="nature">Nature</label>
                <select class="form-control @error('nature') is-invalid @enderror" id="nature" placeholder="" name="nature">
                    <option value="cash">Cash</option>
                    <option value="bank">Bank transfer / Mobile Money</option>
                </select>
            </div>
            <input type="hidden" name="type" value="expense">
            <input type="hidden" name="name" value="{{ auth()->user()->name }}" id="username">
            <input type="hidden" name="email" value="{{ auth()->user()->email }}" id="email">
            <input type="hidden" name="phone" value="{{ auth()->user()->phone }}" id="phone">
            <div>
                <label for="amount">Amount</label>
                <input type="text" class="form-control @error('amount') is-invalid @enderror" id="amount" placeholder="" name="amount" required="required">
            </div>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" name="send" id="send">

                <label class="form-check-label" for="send">
                    Send money from this app
                </label>
            </div>
            <hr>
            <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary btn-lg" id="submitexpenseform">Save</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="editmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Edit Expense</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="/shop/edititem" method="post" enctype="multipart/form-data">
            @csrf
            @method('PATCH')
            <div class="form-group">
                <label for="name">Name / Title</label>
                <input type="text" class="form-control @error('name') is-invalid @enderror" required="required" id="ename" placeholder="" name="name">
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <textarea class="form-control @error('description') is-invalid @enderror" required="required" id="edescription" name="description" value=""></textarea>
            </div>
            <div class="form-group">
                <label for="nature">Nature</label>
                <select class="form-control @error('nature') is-invalid @enderror" required="required" id="enature" placeholder="" name="nature">
                    <option value="cash">Cash</option>
                    <option value="bank">Bank transfer / Mobile Money</option>
                </select>
            </div>
            <input type="hidden" name="type" value="expense">
            <div>
                <label for="amount">Amount</label>
                <input type="text" class="form-control @error('amount') is-invalid @enderror" required="required" id="eamount" placeholder="" name="amount">
            </div>
            <input type="hidden" name="id" id="eid">
            <hr>
            <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary btn-lg">Save</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="deletemodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Delete Expense?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="/shop/deleteitem" method="post">
            @csrf
            @method('DELETE')
            Are you sure you want to delete this expense?
            <input type="hidden" name="id" id="did">
            <hr>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">No, Cancel</button>
            <button type="submit" class="btn btn-primary">Yes, Delete</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade show" id="main-popup-search" tabindex="-1" role="dialog" aria-labelledby="main-popup-search" style="display: none;" aria-modal="true">
    <div class="modal-dialog modal-dialog-centered window-popup main-popup-search" role="document">
        <div class="modal-content">
            <a href="#" class="close icon-close" data-dismiss="modal" aria-label="Close">
                <svg class="olymp-close-icon"><use xlink:href="#olymp-close-icon"></use></svg>
            </a>
            <div class="modal-body">
                <form class="form-inline search-form" method="post" action="/search">
                    @csrf
                    <div class="form-group label-floating is-empty">
                        <label class="control-label">What are you looking for?</label>
                        <input class="form-control bg-white" placeholder="" type="search" value="" name="barcode">
                    <span class="material-input"></span></div>
                    <button class="btn btn-success btn-lg" type="submit">Search</button>
                    <button type="button" class="btn btn-danger btn-lg" data-dismiss="modal">Cancel</button>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="{{ URL::asset('js/jQuery/jquery-3.4.1.js') }}"></script>
<script src="{{ URL::asset('print.min.js') }}"></script>
<script src="{{ URL::asset('js/sweetalert.js') }}"></script>
<script src="https://api.ravepay.co/flwv3-pug/getpaidx/api/flwpbf-inline.js"></script>
<script src="{{ URL::asset('js/main.js') }}"></script>
<script src="{{ URL::asset('js/libs-init/libs-init.js') }}"></script>
<script defer src="{{ URL::asset('fonts/fontawesome-all.js') }}"></script>
<script src="{{ URL::asset('js/libs/Headroom.js') }}"></script>
<script src="{{ URL::asset('js/libs/material.min.js') }}"></script>
<script src="{{ URL::asset('js/libs/bootstrap-select.js') }}"></script>
<script src="{{ URL::asset('js/libs/ion.rangeSlider.js') }}"></script>
<script src="{{ URL::asset('js/libs/perfect-scrollbar.js') }}"></script>
<script src="{{ URL::asset('Bootstrap/dist/js/bootstrap.bundle.js') }}"></script>

</body>
</html>