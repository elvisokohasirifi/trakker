<!DOCTYPE html>
<html lang="en">
<head>

    <title>Trakker - Sales</title>

    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="shortcut icon" type="image/png" href="{{ URL::asset('logo.png') }}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('Bootstrap/dist/css/bootstrap-reboot.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('Bootstrap/dist/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('Bootstrap/dist/css/bootstrap-grid.css') }}">

    <!-- Main Styles CSS -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/main.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/fonts.min.css') }}">

    <!-- Main Font -->
    <script src="{{ URL::asset('js/libs/webfontloader.min.js') }}"></script>
    <script>
        WebFont.load({
            google: {
                families: ['Roboto:300,400,500,700:latin']
            }
        });
    </script>

</head>
<body class="body-bg-white">

<!-- Stunning header -->

<div class="stunning-header bg-primary-opacity">

    
    <!-- Header Standard Landing  -->
    
    <div class="header--standard header--standard-landing" id="header--standard">
        <div class="container">
            <div class="header--standard-wrap">
    
                <a href="#" class="logo">
                    <div class="img-wrap">
                        <img src="{{ URL::asset('logo.png') }}" alt="Trakker" style="width: 50px">
                        <img src="{{ URL::asset('logo.png') }}" alt="Trakker" class="logo-colored" style="width: 50px">
                    </div>
                </a>
    
                <a href="#" class="open-responsive-menu js-open-responsive-menu">
                    <svg class="olymp-menu-icon"><use xlink:href="{{ URL::asset('svg-icons/sprites/icons.svg#olymp-menu-icon') }}"></use></svg>
                </a>
    
                <div class="nav nav-pills nav1 header-menu">
                    <div class="mCustomScrollbar">
                        <ul>
                            <li class="nav-item">
                                <a href="/shop/home" class="nav-link">Home</a>
                            </li>

                            <li class="nav-item">
                                <a href="/shop/products" class="nav-link">Products</a>
                            </li>

                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" data-hover="dropdown" data-toggle="dropdown" href="javascript:void(0)" role="button" aria-haspopup="false" aria-expanded="false" tabindex="1">Transactions</a>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="/shop/sales">Sell</a>
                                    <a href="/shop/revenues" class="dropdown-item">Revenues</a>
                                    <a href="/shop/expenses" class="dropdown-item">Expenses</a>
                                </div>
                            </li>

                            <li class="nav-item">
                                <a href="/shop/orders" class="nav-link">Sales</a>
                            </li>

                            <li class="nav-item">
                                <a href="/shop/payments" class="nav-link">Payments</a>
                            </li>

                            @if(auth()->user()->role == 'shopadmin')
                            <li class="nav-item">
                                <a href="/shop/users" class="nav-link">Users</a>
                            </li>
                            @endif

                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" data-hover="dropdown" data-toggle="dropdown" href="javascript:void(0)" role="button" aria-haspopup="false" aria-expanded="false" tabindex="1">Account</a>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="/shop/account">My Account</a>
                                    <a href="/logout" class="dropdown-item">Log Out</a>
                                </div>
                            </li>
                            
                            <li class="close-responsive-menu js-close-responsive-menu">
                                <svg class="olymp-close-icon"><use xlink:href="{{ URL::asset('svg-icons/sprites/icons.svg#olymp-close-icon') }}"></use></svg>
                            </li>
                            <li class="nav-item js-expanded-menu">
                                <a href="#" class="nav-link">
                                    <svg class="olymp-menu-icon"><use xlink:href="{{ URL::asset('svg-icons/sprites/icons.svg#olymp-menu-icon') }}"></use></svg>
                                    <svg class="olymp-close-icon"><use xlink:href="{{ URL::asset('svg-icons/sprites/icons.svg#olymp-close-icon') }}"></use></svg>
                                </a>
                            </li>
    
                            <li class="menu-search-item">
                                <a href="#" class="nav-link" data-toggle="modal" data-target="#main-popup-search">
                                    <svg class="olymp-magnifying-glass-icon"><use xlink:href="{{ URL::asset('svg-icons/sprites/icons.svg#olymp-magnifying-glass-icon') }}"></use></svg>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- ... end Header Standard Landing  -->
    <div class="header-spacer--standard"></div>

    <div class="stunning-header-content">
        <h1 class="stunning-header-title">{{$shop->name}}</h1>
        <ul class="breadcrumbs">
            <li class="breadcrumbs-item">
                <a href="/">Home</a>
                <span class="icon breadcrumbs-custom">/</span>
            </li>
            <li class="breadcrumbs-item active">
                <span>Sales</span>
            </li>
        </ul>
    </div>

    <div class="content-bg-wrap stunning-header-bg2"></div>
</div>

<section class="medium-padding120">
    <div class="container" id="container">
        <div class="row">
            <div class="col col-xl-7 col-lg-7 col-md-6 col-sm-6 col-12">
                
                <div class="crumina-module crumina-heading with-title-decoration">
                    <h5 class="heading-title">Products</h5>
                </div>
            
                <div class="row">
                    @forelse($products as $product)
                    <div class="col col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12">
                        <div class="ui-block">
                            
                            <div class="birthday-item inline-items">
                                <div class="author-thumb">
                                    <img src="{{ URL::asset('/img/bg-event-day.jpg') }}" alt="item" style="width: 40px; height: 40px">
                                </div>
                                <div class="birthday-author-name">
                                    <a href="#" class="h6 author-name">{{ $product->name }}</a>
                                    <div class="birthday-date">GHS {{ $product->selling_price }} ({{ $product->quantity }} remaining)</div>
                                </div>
                                <button class="btn btn-sm bg-blue" onclick="addtocart({{ $product->id }}, '{{ $product->name }}', {{ $product->selling_price }}, {{ $product->quantity }})">+</button>
                            </div>

                        </div>
                    </div>
                    @empty
                    <p class="pl-3">No products in stock </p>
                    @endforelse
                </div>         

            </div>
            <div class="col col-xl-4 col-lg-4 col-md-6 col-sm-6 col-12 ml-auto">
                    <div class="crumina-module crumina-heading with-title-decoration">
                        <h5 class="heading-title">Orders</h5>
                    </div>

                <table class="shop_table cart" style="width: 100%">
                        <thead>
                        <tr>
                            <th class="product-thumbnail">ITEM</th>
                            <th class="product-price">PRICE</th>
                            <th class="product-quantity">QTY</th>
                            <th class="product-subtotal">TOTAL</th>
                            <th class="product-remove" style="text-align: right">REMOVE</th>
                        </tr>
                        </thead>
                        <tbody id="cartitems">
                       
                        </tbody>
                    </table>

                    <div class="crumina-module crumina-heading with-title-decoration pt-3">
                        <h5 class="heading-title">Totals</h5>
                    </div>
                    <ul class="order-totals-list">
                        <li class="total">
                            Order Total <span>GHS <b id="ordertotal"></b></span>
                        </li>
                    </ul>
                    
                    <ul class="payment-methods-list">
                        <li>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="optionsRadios" value="bank">
                                    Bank Transfer / Mobile Money
                                </label>
                            </div>
                            <p>Includes all non-cash payment forms.</p>
                        </li>
                        <li>
                            <div class="radio">
                                <label>
                                    <input type="radio" name="optionsRadios" checked="checked" value="cash">
                                    Cash
                                </label>
                            </div>
                        </li>
                    </ul>
                    
                    <!-- ... end Payment Methods List -->

                    <button class="btn btn-purple btn-lg full-width" onclick="completeorder()">Complete</button>
            </div>
        </div>
    </div>
</section>


<script src="{{ URL::asset('js/jQuery/jquery-3.4.1.js') }}"></script>
<script src="{{ URL::asset('js/sweetalert.js') }}"></script>
<script src="{{ URL::asset('js/main.js') }}"></script>
<script src="{{ URL::asset('js/libs-init/libs-init.js') }}"></script>
<script defer src="{{ URL::asset('fonts/fontawesome-all.js') }}"></script>
<script src="{{ URL::asset('js/libs/Headroom.js') }}"></script>
<script src="{{ URL::asset('js/libs/material.min.js') }}"></script>
<script src="{{ URL::asset('js/libs/bootstrap-select.js') }}"></script>
<script src="{{ URL::asset('js/libs/ion.rangeSlider.js') }}"></script>
<script src="{{ URL::asset('js/libs/perfect-scrollbar.js') }}"></script>
<script src="{{ URL::asset('Bootstrap/dist/js/bootstrap.bundle.js') }}"></script>

</body>
</html>