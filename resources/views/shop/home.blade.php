<!DOCTYPE html>
<html lang="en">
<head>

    <title>Trakker - Home</title>

    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <link rel="shortcut icon" type="image/png" href="{{ URL::asset('logo.png') }}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('Bootstrap/dist/css/bootstrap-reboot.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('Bootstrap/dist/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('Bootstrap/dist/css/bootstrap-grid.css') }}">

    <!-- Main Styles CSS -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/main.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/fonts.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/Chart.css') }}">

    <!-- Main Font -->
    <script src="{{ URL::asset('js/libs/webfontloader.min.js') }}"></script>
    <script>
        WebFont.load({
            google: {
                families: ['Roboto:300,400,500,700:latin']
            }
        });
    </script>

</head>
<body class="body-bg-white">

<!-- Stunning header -->

<div class="stunning-header bg-primary-opacity">

    
    <!-- Header Standard Landing  -->
    
    <div class="header--standard header--standard-landing" id="header--standard">
        <div class="container">
            <div class="header--standard-wrap">
    
                <a href="#" class="logo">
                    <div class="img-wrap">
                        <img src="{{ URL::asset('logo.png') }}" alt="Trakker" style="width: 50px">
                        <img src="{{ URL::asset('logo.png') }}" alt="Trakker" class="logo-colored" style="width: 50px">
                    </div>
                </a>
    
                <a href="#" class="open-responsive-menu js-open-responsive-menu">
                    <svg class="olymp-menu-icon"><use xlink:href="{{ URL::asset('svg-icons/sprites/icons.svg#olymp-menu-icon') }}"></use></svg>
                </a>
    
                <div class="nav nav-pills nav1 header-menu">
                    <div class="mCustomScrollbar">
                        <ul>
                            <li class="nav-item">
                                <a href="/shop/home" class="nav-link">Home</a>
                            </li>

                            <li class="nav-item">
                                <a href="/shop/products" class="nav-link">Products</a>
                            </li>

                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" data-hover="dropdown" data-toggle="dropdown" href="javascript:void(0)" role="button" aria-haspopup="false" aria-expanded="false" tabindex="1">Transactions</a>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="/shop/sales">Sell</a>
                                    <a href="/shop/revenues" class="dropdown-item">Revenues</a>
                                    <a href="/shop/expenses" class="dropdown-item">Expenses</a>
                                </div>
                            </li>

                            <li class="nav-item">
                                <a href="/shop/orders" class="nav-link">Sales</a>
                            </li>

                            <li class="nav-item">
                                <a href="/shop/payments" class="nav-link">Payments</a>
                            </li>

                            @if(auth()->user()->role == 'shopadmin')
                            <li class="nav-item">
                                <a href="/shop/users" class="nav-link">Users</a>
                            </li>
                            @endif

                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" data-hover="dropdown" data-toggle="dropdown" href="javascript:void(0)" role="button" aria-haspopup="false" aria-expanded="false" tabindex="1">Account</a>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="/shop/account">My Account</a>
                                    <a href="/logout" class="dropdown-item">Log Out</a>
                                </div>
                            </li>
                            
                            <li class="close-responsive-menu js-close-responsive-menu">
                                <svg class="olymp-close-icon"><use xlink:href="{{ URL::asset('svg-icons/sprites/icons.svg#olymp-close-icon') }}"></use></svg>
                            </li>
                            <li class="nav-item js-expanded-menu">
                                <a href="#" class="nav-link">
                                    <svg class="olymp-menu-icon"><use xlink:href="{{ URL::asset('svg-icons/sprites/icons.svg#olymp-menu-icon') }}"></use></svg>
                                    <svg class="olymp-close-icon"><use xlink:href="{{ URL::asset('svg-icons/sprites/icons.svg#olymp-close-icon') }}"></use></svg>
                                </a>
                            </li>
    
                            <li class="menu-search-item">
                                <a href="#" class="nav-link" data-toggle="modal" data-target="#main-popup-search">
                                    <svg class="olymp-magnifying-glass-icon"><use xlink:href="{{ URL::asset('svg-icons/sprites/icons.svg#olymp-magnifying-glass-icon') }}"></use></svg>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- ... end Header Standard Landing  -->
    <div class="header-spacer--standard"></div>

    <div class="stunning-header-content">
        <h1 class="stunning-header-title">{{$shop->name}}</h1>
        <ul class="breadcrumbs">
            <li class="breadcrumbs-item">
                <a href="/">Home</a>
                <span class="icon breadcrumbs-custom">/</span>
            </li>
        </ul>
    </div>

    <div class="content-bg-wrap stunning-header-bg2"></div>
</div>

<section class="medium-padding100">
    <div class="container">
        <div class="row">
            @if(Auth::user()->role == 'shopadmin')
            <div class="col col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                <a href="/shop/users"><div class="card text-white bg-primary mb-3" style="max-width: 20rem;">
                  <div class="card-body">
                    <h5 class="card-title text-white">Registered Attendants</h5>
                    <h1 class="card-text day-number text-white">{{ $user }}</h1>
                  </div>
                </div></a>
            </div>
            @endif
            <div class="col col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                <a href="/shop/expenses"><div class="card text-white mb-3" style="max-width: 20rem;background-color: #ff5e3a">
                  <div class="card-body">
                    <h5 class="card-title text-white">Expenses</h5>
                    <h1 class="card-text day-number text-white">GHS {{ $expenses }}</h1>
                  </div>
                </div></a>
            </div>
            <div class="col col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                <a href="/shop/revenues"><div class="card text-white bg-success mb-3" style="max-width: 20rem;">
                  <div class="card-body">
                    <h5 class="card-title text-white">Revenues</h5>
                    <h1 class="card-text day-number text-white">GHS {{ $revenues }}</h1>
                  </div>
                </div></a>
            </div>
            <div class="col col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                <a href="/shop/products"><div class="card text-white mb-3" style="max-width: 20rem; background-color: #7c5ac2">
                  <div class="card-body">
                    <h5 class="card-title text-white">Products</h5>
                    <h1 class="card-text day-number text-white">{{ $products }}</h1>
                  </div>
                </div></a>
            </div>
            <div class="col col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                <a href="/shop/orders"><div class="card text-white mb-3" style="max-width: 20rem; background-color: #666">
                  <div class="card-body">
                    <h5 class="card-title text-white">Sales</h5>
                    <h1 class="card-text day-number text-white">{{ $orders }}</h1>
                  </div>
                </div></a>
            </div>
            <div class="col col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                <div class="card text-white mb-3" style="max-width: 20rem; background-color: blue">
                  <div class="card-body">
                    <h5 class="card-title text-white">Total Sales</h5>
                    <h1 class="card-text day-number text-white">GHS {{ $sales[0] }}</h1>
                  </div>
                </div>
            </div>
            <div class="col col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                <div class="card text-white mb-3" style="max-width: 20rem; background-color: brown">
                  <div class="card-body">
                    <h5 class="card-title text-white">Cost of Sales</h5>
                    <h1 class="card-text day-number text-white">GHS {{ $costofsales[0] }}</h1>
                  </div>
                </div>
            </div>
            <div class="col col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                <div class="card text-white mb-3" style="max-width: 20rem; background-color: black">
                  <div class="card-body">
                    <h5 class="card-title text-white">Gross Profit</h5>
                    <h1 class="card-text day-number text-white">GHS {{ $sales[0] - $costofsales[0] }}</h1>
                  </div>
                </div>
            </div>
            <div class="col col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12">
                <div class="card text-white mb-3" style="max-width: 20rem; background-color: green">
                  <div class="card-body">
                    <h5 class="card-title text-white">Net Profit</h5>
                    <h1 class="card-text day-number text-white">GHS {{ $sales[0] + $revenues - $costofsales[0] - $expenses}}</h1>
                  </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="small-padding100" style="padding-bottom: 30px">
    <div class="container">
        <div class="row">
            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                <canvas id="myChart" style="width: 100%; height: 500px">
                    
                </canvas>
            </div>
        </div>
    </div>
</section>

<script src="{{ URL::asset('js/jQuery/jquery-3.4.1.js') }}"></script>
<script src="{{ URL::asset('js/sweetalert.js') }}"></script>
<script src="{{ URL::asset('js/main.js') }}"></script>
<script src="{{ URL::asset('js/libs-init/libs-init.js') }}"></script>
<script defer src="{{ URL::asset('fonts/fontawesome-all.js') }}"></script>
<script src="{{ URL::asset('js/libs/Headroom.js') }}"></script>
<script src="{{ URL::asset('js/libs/material.min.js') }}"></script>
<script src="{{ URL::asset('js/libs/bootstrap-select.js') }}"></script>
<script src="{{ URL::asset('js/libs/ion.rangeSlider.js') }}"></script>
<script src="{{ URL::asset('js/libs/perfect-scrollbar.js') }}"></script>
<script src="{{ URL::asset('Bootstrap/dist/js/bootstrap.bundle.js') }}"></script>
<script src="{{ URL::asset('js/Chart.bundle.js') }}"></script>
<script type="text/javascript">
    new Chart(document.getElementById("myChart"), {
    type: 'bar',
    data: {
      labels: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
      datasets: [
        {
          label: "Expenses",
          backgroundColor: 'rgba(255, 99, 132, 0.8)',
          data: expanddata(<?php echo($allexpenses);?>)
        }, {
          label: "Revenues",
          backgroundColor: 'rgba(54, 162, 235, 0.8)',
          data: expanddata(<?php echo($allrevenues);?>)
        }, {
          label: "Sales",
          backgroundColor: "rgba(255, 206, 86, 0.8)",
          data: expanddata(<?php echo($allsales);?>)
        }
      ]
    },
    options: {
      title: {
        display: true,
        text: 'Monthly Report'
      }
    }
});

    //console.log(<?php echo($allexpenses);?>);

    function expanddata(val){
        let data = [0,0,0,0,0,0,0,0,0,0,0,0];
        for (var i = val.length - 1; i >= 0; i--) {
            data[val[i]['month'] -1 ] = val[i]['total'];
        }
        return data;
    }

    /*console.log(expanddata(<?php echo($allexpenses);?>));
    console.log(expanddata(<?php echo($allrevenues);?>));
    console.log(expanddata(<?php echo($allsales);?>));*/
</script>

</body>
</html>