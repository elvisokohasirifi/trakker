
<!DOCTYPE html>
<html lang="en">
<head>

    <title>Trakker - Password</title>

    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <link rel="shortcut icon" type="image/png" href="{{ URL::asset('logo.png') }}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('Bootstrap/dist/css/bootstrap-reboot.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('Bootstrap/dist/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('Bootstrap/dist/css/bootstrap-grid.css') }}">

    <!-- Main Styles CSS -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/main.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/fonts.min.css') }}">

    <!-- Main Font -->
    <script src="{{ URL::asset('js/libs/webfontloader.min.js') }}"></script>
    <script>
        WebFont.load({
            google: {
                families: ['Roboto:300,400,500,700:latin']
            }
        });
    </script>

</head>
<body class="body-bg-white">

<!-- Stunning header -->

<div class="stunning-header bg-primary-opacity">

    
    <!-- Header Standard Landing  -->
    
    <div class="header--standard header--standard-landing" id="header--standard">
        <div class="container">
            <div class="header--standard-wrap">
    
                <a href="#" class="logo">
                    <div class="img-wrap">
                        <img src="{{ URL::asset('logo.png') }}" alt="Trakker" style="width: 50px">
                        <img src="{{ URL::asset('logo.png') }}" alt="Trakker" class="logo-colored" style="width: 50px">
                    </div>
                </a>
    
                <a href="#" class="open-responsive-menu js-open-responsive-menu">
                    <svg class="olymp-menu-icon"><use xlink:href="{{ URL::asset('svg-icons/sprites/icons.svg#olymp-menu-icon') }}"></use></svg>
                </a>
    
                <div class="nav nav-pills nav1 header-menu">
                    <div class="mCustomScrollbar">
                        <ul>
                            <li class="nav-item">
                                <a href="/shop/home" class="nav-link">Home</a>
                            </li>

                            <li class="nav-item">
                                <a href="/shop/products" class="nav-link">Products</a>
                            </li>

                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" data-hover="dropdown" data-toggle="dropdown" href="javascript:void(0)" role="button" aria-haspopup="false" aria-expanded="false" tabindex="1">Transactions</a>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="/shop/sales">Sell</a>
                                    <a href="/shop/revenues" class="dropdown-item">Revenues</a>
                                    <a href="/shop/expenses" class="dropdown-item">Expenses</a>
                                </div>
                            </li>

                            <li class="nav-item">
                                <a href="/shop/orders" class="nav-link">Sales</a>
                            </li>

                            <li class="nav-item">
                                <a href="/shop/payments" class="nav-link">Payments</a>
                            </li>

                            @if(auth()->user()->role == 'shopadmin')
                            <li class="nav-item">
                                <a href="/shop/users" class="nav-link">Users</a>
                            </li>
                            @endif

                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" data-hover="dropdown" data-toggle="dropdown" href="javascript:void(0)" role="button" aria-haspopup="false" aria-expanded="false" tabindex="1">Account</a>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="/shop/account">My Account</a>
                                    <a href="/logout" class="dropdown-item">Log Out</a>
                                </div>
                            </li>
                            
                            <li class="close-responsive-menu js-close-responsive-menu">
                                <svg class="olymp-close-icon"><use xlink:href="{{ URL::asset('svg-icons/sprites/icons.svg#olymp-close-icon') }}"></use></svg>
                            </li>
                            <li class="nav-item js-expanded-menu">
                                <a href="#" class="nav-link">
                                    <svg class="olymp-menu-icon"><use xlink:href="{{ URL::asset('svg-icons/sprites/icons.svg#olymp-menu-icon') }}"></use></svg>
                                    <svg class="olymp-close-icon"><use xlink:href="{{ URL::asset('svg-icons/sprites/icons.svg#olymp-close-icon') }}"></use></svg>
                                </a>
                            </li>
    
                            <li class="menu-search-item">
                                <a href="#" class="nav-link" data-toggle="modal" data-target="#main-popup-search">
                                    <svg class="olymp-magnifying-glass-icon"><use xlink:href="{{ URL::asset('svg-icons/sprites/icons.svg#olymp-magnifying-glass-icon') }}"></use></svg>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	<!-- ... end Header Standard Landing  -->
	<div class="header-spacer--standard"></div>

	<div class="stunning-header-content">
		<h1 class="stunning-header-title">{{ $user->name }}</h1>
		<ul class="breadcrumbs">
			<li class="breadcrumbs-item">
				<a href="#">Home</a>
				<span class="icon breadcrumbs-custom">/</span>
			</li>
			<li class="breadcrumbs-item active">
				<span>Password</span>
			</li>
		</ul>
	</div>

	<div class="content-bg-wrap stunning-header-bg1"></div>
</div>

<div class="container">
	<div class="row">
		<div class="col col-xl-9 order-xl-2 col-lg-9 order-lg-2 col-md-12 order-md-1 col-sm-12 col-12">
			<div class="ui-block">
				
				<div class="ui-block">
				<div class="ui-block-title">
					<h6 class="title">Change Password</h6>
				</div>
				<div class="ui-block-content">
					@if (session('error'))
                        <div class="alert alert-danger">
                            {{ session('error') }}
                        </div>
                    @endif
                    @if (session('success'))
                        <div class="alert alert-success">
                            {{ session('success') }}
                        </div>
                	@endif

                	<ul>
			             @foreach ($errors->all() as $error)
			                 <li class="alert alert-danger">{{ $error }}</li>
			             @endforeach
			        </ul>
					
					<!-- Change Password Form -->
					
					<form method="POST" action="/account-password/update">
                        @csrf
						<div class="row">
					
							<div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
								<div class="form-group label-floating">
									<label class="control-label">Confirm Current Password</label>
									<input class="form-control" placeholder="" type="password" value="******" id="curpass" name="curpass">
								</div>
							</div>
					
							<div class="col col-lg-6 col-md-6 col-sm-12 col-12">
								<div class="form-group label-floating is-empty">
									<label class="control-label">Your New Password</label>
									<input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

	                                @error('password')
	                                    <span class="invalid-feedback" role="alert">
	                                        <strong>{{ $message }}</strong>
	                                    </span>
	                                @enderror
								</div>
							</div>
							<div class="col col-lg-6 col-md-6 col-sm-12 col-12">
								<div class="form-group label-floating is-empty">
									<label class="control-label">Confirm New Password</label>
									<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
								</div>
							</div>
					
							<div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
								<button type="submit" class="btn btn-primary btn-lg full-width">
                                    {{ __('Reset Password') }}
                                </button>
							</div>
							
						</div>
					</form>
					
					
					<!-- ... end Change Password Form -->
				</div>
			</div>
			</div>
		</div>

		<div class="col col-xl-3 order-xl-1 col-lg-3 order-lg-1 col-md-12 order-md-2 col-sm-12 col-12 ">
			<div class="ui-block">

				<!-- Your Profile  -->
				
				<div class="your-profile">
					<div class="ui-block-title ui-block-title-small">
						<h6 class="title">Your PROFILE</h6>
					</div>
				
					<div id="accordion" role="tablist" aria-multiselectable="true">
						<div class="card">
							<div class="card-header" role="tab" id="headingOne">
								<h6 class="mb-0">
									<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
										Account Settings
										<svg class="olymp-dropdown-arrow-icon"><use xlink:href="#olymp-dropdown-arrow-icon"></use></svg>
									</a>
								</h6>
							</div>
				
							<div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="headingOne">
								<ul class="your-profile-menu">
									<li>
										<a href="/shop/account">Personal Information</a>
									</li>
									<li>
										<a href="#">Change Password</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				
				</div>
				
				<!-- ... end Your Profile  -->

			</div>
		</div>
	</div>
</div>

<script src="{{ URL::asset('js/jQuery/jquery-3.4.1.js') }}"></script>
<script src="{{ URL::asset('js/main.js') }}"></script>
<script src="{{ URL::asset('js/libs-init/libs-init.js') }}"></script>
<script defer src="{{ URL::asset('fonts/fontawesome-all.js') }}"></script>
<script src="{{ URL::asset('js/libs/Headroom.js') }}"></script>
<script src="{{ URL::asset('js/libs/material.min.js') }}"></script>
<script src="{{ URL::asset('js/libs/bootstrap-select.js') }}"></script>
<script src="{{ URL::asset('js/libs/ion.rangeSlider.js') }}"></script>
<script src="{{ URL::asset('js/libs/perfect-scrollbar.js') }}"></script>
<script src="{{ URL::asset('Bootstrap/dist/js/bootstrap.bundle.js') }}"></script>

<script type="text/javascript">
	function show(name, email, createdat, deviceid){
		document.getElementById('namepart').innerHTML = name;
		document.getElementById('recipientname').value = name;
		document.getElementById('emailpart').innerHTML = email;
		document.getElementById('email').value = email;
		document.getElementById('deviceid').value = deviceid;
		document.getElementById('createdatpart').innerHTML = 'Registered ' + createdat;
		if (deviceid != '') {
			var s = '<option value="email">Email</option>\
					<option value="push">Push Notification</option>';
			document.getElementById('notiftype').innerHTML = s;
		}
		else{
			var s = '<option value="email">Email</option>';
			document.getElementById('notiftype').innerHTML = s;
		}
	}
</script>

</body>
</html>