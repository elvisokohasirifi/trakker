<!DOCTYPE html>
<html lang="en">
<head>

    <title>Trakker - Users</title>

    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <link rel="shortcut icon" type="image/png" href="{{ URL::asset('logo.png') }}">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('Bootstrap/dist/css/bootstrap-reboot.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('Bootstrap/dist/css/bootstrap.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('Bootstrap/dist/css/bootstrap-grid.css') }}">

    <!-- Main Styles CSS -->
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/main.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/fonts.min.css') }}">

    <!-- Main Font -->
    <script src="{{ URL::asset('js/libs/webfontloader.min.js') }}"></script>
    <script>
        WebFont.load({
            google: {
                families: ['Roboto:300,400,500,700:latin']
            }
        });
    </script>

</head>
<body class="body-bg-white">

<!-- Stunning header -->

<div class="stunning-header bg-primary-opacity">

    
    <!-- Header Standard Landing  -->
    
    <div class="header--standard header--standard-landing" id="header--standard">
        <div class="container">
            <div class="header--standard-wrap">
    
                <a href="#" class="logo">
                    <div class="img-wrap">
                        <img src="{{ URL::asset('logo.png') }}" alt="Trakker" style="width: 50px">
                        <img src="{{ URL::asset('logo.png') }}" alt="Trakker" class="logo-colored" style="width: 50px">
                    </div>
                </a>
    
                <a href="#" class="open-responsive-menu js-open-responsive-menu">
                    <svg class="olymp-menu-icon"><use xlink:href="{{ URL::asset('svg-icons/sprites/icons.svg#olymp-menu-icon') }}"></use></svg>
                </a>
    
                <div class="nav nav-pills nav1 header-menu">
                    <div class="mCustomScrollbar">
                        <ul>
                            <li class="nav-item">
                                <a href="/shop/home" class="nav-link">Home</a>
                            </li>

                            <li class="nav-item">
                                <a href="/shop/products" class="nav-link">Products</a>
                            </li>

                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" data-hover="dropdown" data-toggle="dropdown" href="javascript:void(0)" role="button" aria-haspopup="false" aria-expanded="false" tabindex="1">Transactions</a>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="/shop/sales">Sell</a>
                                    <a href="/shop/revenues" class="dropdown-item">Revenues</a>
                                    <a href="/shop/expenses" class="dropdown-item">Expenses</a>
                                </div>
                            </li>

                            <li class="nav-item">
                                <a href="/shop/orders" class="nav-link">Sales</a>
                            </li>

                            <li class="nav-item">
                                <a href="/shop/payments" class="nav-link">Payments</a>
                            </li>

                            @if(auth()->user()->role == 'shopadmin')
                            <li class="nav-item">
                                <a href="/shop/users" class="nav-link">Users</a>
                            </li>
                            @endif

                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" data-hover="dropdown" data-toggle="dropdown" href="javascript:void(0)" role="button" aria-haspopup="false" aria-expanded="false" tabindex="1">Account</a>
                                <div class="dropdown-menu">
                                    <a class="dropdown-item" href="/shop/account">My Account</a>
                                    <a href="/logout" class="dropdown-item">Log Out</a>
                                </div>
                            </li>
                            
                            <li class="close-responsive-menu js-close-responsive-menu">
                                <svg class="olymp-close-icon"><use xlink:href="{{ URL::asset('svg-icons/sprites/icons.svg#olymp-close-icon') }}"></use></svg>
                            </li>
                            <li class="nav-item js-expanded-menu">
                                <a href="#" class="nav-link">
                                    <svg class="olymp-menu-icon"><use xlink:href="{{ URL::asset('svg-icons/sprites/icons.svg#olymp-menu-icon') }}"></use></svg>
                                    <svg class="olymp-close-icon"><use xlink:href="{{ URL::asset('svg-icons/sprites/icons.svg#olymp-close-icon') }}"></use></svg>
                                </a>
                            </li>
    
                            <li class="menu-search-item">
                                <a href="#" class="nav-link" data-toggle="modal" data-target="#main-popup-search">
                                    <svg class="olymp-magnifying-glass-icon"><use xlink:href="{{ URL::asset('svg-icons/sprites/icons.svg#olymp-magnifying-glass-icon') }}"></use></svg>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <!-- ... end Header Standard Landing  -->
    <div class="header-spacer--standard"></div>

    <div class="stunning-header-content">
        <h1 class="stunning-header-title">{{$shop->name}}</h1>
        <ul class="breadcrumbs">
            <li class="breadcrumbs-item">
                <a href="/">Home</a>
                <span class="icon breadcrumbs-custom">/</span>
            </li>
            <li class="breadcrumbs-item active">
                <span>Users</span>
            </li>
        </ul>
    </div>

    <div class="content-bg-wrap stunning-header-bg2"></div>
</div>

<section class="medium-padding100">
    <div class="container">
        @if (session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif
        <ul>
             @foreach ($errors->all() as $error)
                 <li class="alert alert-danger">{{ $error }}</li>
             @endforeach
        </ul>
        @if(Auth::user()->role == 'shopadmin')
            <button class="btn btn-primary" data-toggle="modal" data-target="#addmodal">Add New Attendant</button>
        @endif
        <div class="row">
            <div class="col col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 m-auto table-responsive">
                
                <table class="table table-striped table-responsive">
                    <tr class="head">
                        <th>ROLE</th>
                        <th>FULL NAME</th>
                        <th>EMAIL</th>
                        <th>PHONE</th>
                        @if(Auth::user()->role == 'shopadmin')
                        <th>CHANGE</th>
                        @endif
                    </tr>
                
                    @forelse($users as $user)

                    <tr>
                        <td class="date">{{ $user->role }}</td>
                        <td class="position bold"><a href="/user/{{ $user->id }}">{{ $user->name }}</a></td>
                        <td class="type">{{ $user->email }}</td>
                        <td class="town-place">{{ $user->phone }}</td>
                        @if(Auth::user()->role == 'shopadmin')
                        <td>
                            @if($user->role == 'shopadmin')
                            <button class="btn btn-success btn-sm" disabled="disabled">Delete Account</button>
                            @else
                             <button class="btn btn-success btn-sm" data-toggle="modal" data-target="#deleteattendant" onclick="document.getElementById('did').value = {{ $user->id }}">Delete Account</button>
                            @endif
                        </td>
                        @endif
                        
                    </tr>

                    @empty
                    <p>No users have been registered yet</p>
                    @endforelse
            
                </table>

                {{ $users->links() }}

            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="addmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Register New User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="/shop/addattendant" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="name">Name</label>
                <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" placeholder="" name="name">
            </div>
            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" placeholder="" name="email">
            </div>
            <div class="form-group">
                <label for="phone">Phone</label>
                <input type="tel" class="form-control @error('phone') is-invalid @enderror" id="phone" placeholder="" name="phone">
            </div>
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" placeholder="" name="password">
            </div>
            <hr>
            <button type="button" class="btn btn-secondary btn-lg" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-primary btn-lg">Save</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="deleteattendant" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Delete Account?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="/shop/deleteattendant" method="post">
            @csrf
            @method('DELETE')
            Are you sure you want to delete this attendant?
            <input type="hidden" name="id" id="did">
            <hr>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">No, Cancel</button>
            <button type="submit" class="btn btn-primary">Yes, Delete</button>
        </form>
      </div>
    </div>
  </div>
</div>

<script src="{{ URL::asset('js/jQuery/jquery-3.4.1.js') }}"></script>
<script src="{{ URL::asset('js/sweetalert.js') }}"></script>
<script src="{{ URL::asset('js/main.js') }}"></script>
<script src="{{ URL::asset('js/libs-init/libs-init.js') }}"></script>
<script defer src="{{ URL::asset('fonts/fontawesome-all.js') }}"></script>
<script src="{{ URL::asset('js/libs/Headroom.js') }}"></script>
<script src="{{ URL::asset('js/libs/material.min.js') }}"></script>
<script src="{{ URL::asset('js/libs/bootstrap-select.js') }}"></script>
<script src="{{ URL::asset('js/libs/ion.rangeSlider.js') }}"></script>
<script src="{{ URL::asset('js/libs/perfect-scrollbar.js') }}"></script>
<script src="{{ URL::asset('Bootstrap/dist/js/bootstrap.bundle.js') }}"></script>

</body>
</html>