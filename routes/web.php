<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('/addshop', 'ShopController@addshop');

Route::patch('/editshop', 'ShopController@editshop');

Route::delete('/deleteshop', 'ShopController@deleteshop');

Route::get('/account', 'HomeController@loadaccountuser');

/*test pdf generator*/

Route::post('/printreceipt', 'OrdersController@generatepdf');

Route::post('/search', 'OrdersController@search');

Route::get('/changepassword', 'HomeController@changepassword');

Route::post('/account-password/update', 'HomeController@update');

Route::post('/account-personal/edit', 'HomeController@edit');

/************ SHOP ***************/

//Route::post('/addattendant', 'ShopController@addshop');

Route::get('/shop/home', 'HomeController@shophome')->name('home');

Route::get('/shop/sales', 'OrdersController@sales')->name('sales');

Route::post('/shop/addattendant', 'ShopController@addattendant');

Route::get('/shop/users', 'HomeController@users');

Route::get('/shop/products', 'ProductsController@index');

Route::get('/shop/payments', 'PaymentsController@index');

Route::get('/shop/expenses', 'ItemController@expenses');
Route::post('/shop/addexpense', 'ItemController@addexpense');

Route::delete('/shop/deleteitem', 'ItemController@deleteitem');
Route::patch('/shop/edititem', 'ItemController@edititem');

Route::get('/shop/revenues', 'ItemController@revenues');
Route::post('/shop/addrevenue', 'ItemController@addrevenue');

Route::get('/shop/orders', 'OrdersController@index');

Route::post('/shop/completeorder', 'OrdersController@completeorder');

Route::post('/shop/completepayment', 'PaymentsController@completepayment');

Route::post('/vieworder', 'OrdersController@vieworder');

Route::post('/updateorder', 'OrdersController@updateorder');

Route::delete('/shop/deleteattendant', 'ShopController@deleteattendant');

Route::post('/shop/addproduct', 'ProductsController@addproduct');

Route::patch('/shop/editproduct', 'ProductsController@editproduct');

Route::delete('/shop/deleteproduct', 'ProductsController@deleteproduct');

Route::get('/shop/account', 'HomeController@loadaccountuser');

Route::get('/shop/changepassword', 'HomeController@changepassword');

Route::post('/shop/account-password/update', 'HomeController@update');

Route::post('/shop/account-personal/edit', 'HomeController@edit');

Route::get('/logout', function(){
    Session::flush();
    Auth::logout();
    return Redirect::to("/login");
});
