<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
    protected $fillable = [
        'name', 'contact', 'location', 'description'
    ];

    protected $guarded = [];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
