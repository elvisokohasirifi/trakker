<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    protected $fillable = [
        'name', 'selling_price', 'cost_price', 'shop_id', 'quantity'
    ];

    protected $guarded = [];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
