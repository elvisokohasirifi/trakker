<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use validator;
use App\Item;

class PaymentsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (auth()->user()->role == 'admin') {
            $shops = DB::table('shops')->orderBy('name', 'asc')->latest()->paginate(12);
            return view('home', compact('shops'));
        }
        else{
        	$shop = DB::table('shops')->where('id', '=', auth()->user()->shop_id)->first();
            $payments = DB::table('payments')->join('items', 'items.id', '=', 'payments.item_id')->paginate(15);
            return view('/shop/payments', compact('payments', 'shop'));
        }
    }

    public function completepayment(){
        $data = request()->validate([
            'name' => ['required', 'string', 'max:191'],
            'description' => ['required', 'string'],
            'nature' => ['required', 'string'],
            'amount' => ['required', 'numeric'],
            'payment_no' => ['required', 'numeric'],
            'payment_method' => ['required', 'string']
        ]);

        $tit = str_replace("'","’",$data['name']);
        $tit = str_replace("\"","“",$tit);

        $tit2 = str_replace("'","’",$data['description']);
        $tit2 = str_replace("\"","“",$tit2);

        $id = auth()->user()->items()->create([
            'name' => $tit,
            'description' => $tit2,
            'nature' => $data['nature'],
            'type' => 'expense',
            'shop_id' => auth()->user()->shop_id,
            'amount' => $data['amount']
        ])->id;

        $item = Item::findOrFail($id);
        $item->payments()->create([
            'payment_no' => $data['payment_no'],
            'payment_method' => $data['payment_method'],
            'amount' => $data['amount'],
            'status' => 'Completed'
        ]);
        return response()->json(['success' => 1, 'message' => 1]); 
    }
}
