<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Validator;
use App\Orders;
use App\Products;

class OrdersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        if (auth()->user()->role == 'admin') {
            $shops = DB::table('shops')->orderBy('name', 'asc')->latest()->paginate(12);
            return view('home', compact('shops'));
        }
        else{
        	$shop = DB::table('shops')->where('id', '=', auth()->user()->shop_id)->first();
            $orders = DB::table('orders')->distinct('order_no')->latest()->paginate(10);
            return view('/shop/orders', compact('orders', 'shop'));
        }
    }

    public function sales()
    {
        if (auth()->user()->role == 'admin') {
            $shops = DB::table('shops')->orderBy('name', 'asc')->latest()->paginate(12);
            return view('home', compact('shops'));
        }
        else{
            $shop = DB::table('shops')->where('id', '=', auth()->user()->shop_id)->first();
            $products = DB::table('products')->where('shop_id', '=', auth()->user()->shop_id)->where('quantity', '>', 0)->orderBy('name')->get();
            return view('/shop/sales', compact('products', 'shop'));
        }
    }

    public function search()
    {
        if (auth()->user()->role == 'admin') {
            $shops = DB::table('shops')->orderBy('name', 'asc')->latest()->paginate(12);
            return view('home', compact('shops'));
        }
        else{
            $data = request()->validate([
                'barcode' => ['required', 'string', 'max:13']
            ]);
            $shop = DB::table('shops')->where('id', '=', auth()->user()->shop_id)->first();
            $orders = DB::table('orders')->distinct('order_no')->where('order_no','=',$data['barcode'])->latest()->paginate(10);
            return view('/shop/orders', compact('orders', 'shop'));
        }
    }

    public function vieworder(){
        $validator = Validator::make(request()->all(), [
            'id' => ['required', 'numeric'],
        ]);

        if($validator->fails()){
            return response()->json([
                'success' => 0,
                'message' => $validator->errors()
            ], 201);       
        }   
        $order = Orders::findOrFail(request('id'));
        $details = $order->orderings()->get();
        return response()->json(['success' => 1, 'message' => $details]); 
    }

    public function updateorder(){
        $validator = Validator::make(request()->all(), [
            'id' => ['required', 'numeric'],
        ]);

        if($validator->fails()){
            return response()->json([
                'success' => 0,
                'message' => $validator->errors()
            ], 201);       
        }   
        $order = Orders::findOrFail(request('id'));
        $order->order_status = 'completed';
        $order->save();
        return response()->json(['success' => 1, 'message' => $order->order_no]); 
    }

    public function generatepdf()
    {
        $validator = Validator::make(request()->all(), [
            'message' => ['required', 'string'],
        ]);
        if($validator->fails()){
            return response()->json([
                'success' => 0,
                'message' => $validator->errors()
            ], 201);       
        }  
        $pdf = App::make('dompdf.wrapper');
        $pdf->loadHTML(request('message'));
        return $pdf->stream();
    }

    public function completeorder(){
        $validator = Validator::make(request()->all(), [
            'order_no' => ['required', 'string'],
            'cart' => ['required'],
            'payment' => ['required']
        ]);
        if($validator->fails()){
            return response()->json([
                'success' => 0,
                'message' => $validator->errors()
            ], 201);       
        } 
        $products = request('cart');
        $order = new Orders;
        $order->shop_id = auth()->user()->shop_id;
        $order->order_no = request('order_no');
        $order->payment_method = request('payment');
        $order->order_status = 'Completed';
        $order->save();
        $order = Orders::findOrFail($order->id);

        for ($i=0; $i < count($products); $i++) { 
            $order->orderings()->create([
                'product' => Products::findOrFail($products[$i]['id'])->name,
                'price' => $products[$i]['price'],
                'quantity' => $products[$i]['qty'],
                'cost' => Products::findOrFail($products[$i]['id'])->cost_price
            ]);
            DB::table('products')->where('id', '=', $products[$i]['id'])->decrement('quantity', $products[$i]['qty']);
        }
        return response()->json(['success' => 1, 'message' => 1]); 
    }
}
