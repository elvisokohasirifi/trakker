<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Shop;
use DB;
use Hash;
use App\Products;
use Validator;
use App\Orders;

class APIController extends Controller
{
    public function products(){ 
    	$validator = Validator::make(request()->all(), [
            'id' => ['required', 'numeric'],
        ]);

        if($validator->fails()){
            return response()->json([
                'success' => 0,
                'message' => $validator->errors()
            ], 201);       
        } 	
        $products = DB::table('products')->where('shop_id', '=', request('id'))->get();
        return response()->json(['success' => 1, 'message' => $products]); 
    }

    public function shops(){  
        $shops = DB::table('shops')->get();
        return response()->json(['success' => 1, 'message' => $shops]); 
    }

    public function order(){  
        $validator = Validator::make(request()->all(), [
            'order_id' => ['required', 'numeric'],
            'products' => ['required'],
            'deviceid' => ['required', 'string'],
            'shop_id' => ['required', 'numeric']
        ]);
        if($validator->fails()){
            return response()->json([
                'success' => 0,
                'message' => $validator->errors()
            ], 201);       
        } 
        $products = json_decode(request('products'), true);
        //return response()->json(['success' => 0, 'message' => request('order_id')]); 
        //'order_no', 'payment_status', 'order_status', 'deviceid'
        $order = new Orders;
        $order->shop_id = request('shop_id');
        $order->order_no = request('order_id');
        $order->payment_status = 'pending';
        $order->order_status = 'pending';
        $order->deviceid = request('deviceid');
        $order->save();
        $order = Orders::findOrFail($order->id);

        for ($i=0; $i < count($products); $i++) { 
            $order->orderings()->create([
                'product' => Products::findOrFail($products[$i]['id'])->name,
                'price' => Products::findOrFail($products[$i]['id'])->price,
                'quantity' => $products[$i]['qty']
            ]);
        }
        return response()->json(['success' => 1, 'message' => 1]); 
    }

    public function payment(){
        $validator = Validator::make(request()->all(), [
            'order_no' => ['required', 'numeric'],
            'payment_method' => ['required'],
            'amount' => ['required'],
            'account_number' => ['required', 'numeric'],
            'status' => ['required', 'string']
        ]);
        if($validator->fails()){
            return response()->json([
                'success' => 0,
                'message' => $validator->errors()
            ], 201);       
        } 

        $order = DB::table('orders')->where('order_no', '=', request('order_no'))->first();
        $order->payment()->create([
            'order_no' => $order->order_no,
            'payment_method' => request('payment_method'),
            'amount' => request('amount'),
            'account_number' => request('account_number'),
            'status' => request('status')
        ]);
        $order->payment_status = request('status');
        $order->save();
        sendSingleNotification('Payment status', 'Hello, you payment was '. request('status'), $order->deviceid);
    }
}

function sendMessageSingle($title, $message, $recipient){
    $content = array(
        "en" => $message
        );
    $heading = array(
       "en" => $title
    );
    
    $fields = array(
        'app_id' => "88299293-82c2-4a4f-b95e-df1375f5f45c",
        'include_player_ids' => array($recipient),
        'data' => array("foo" => "bar"),
        'contents' => $content,
        'headings' => $heading
    );
    
    $fields = json_encode($fields);
    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json; charset=utf-8',
                                               'Authorization: Basic NjZmZWQ4MjEtM2RlMy00ZTBjLWFjNDAtN2I5MzIxNjdmNmFm'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);

    $response = curl_exec($ch);
    curl_close($ch);
    
    return 'ok';
}


function sendSingleNotification($title, $message, $id){
    $response = sendMessageSingle($title, $message, $id);
    $return["allresponses"] = $response;
    $return = json_encode( $return);
    
    return $return;
}