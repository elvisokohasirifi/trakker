<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Products;

class ProductsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){
    	if (auth()->user()->role == 'admin') {
            $shops = DB::table('shops')->orderBy('name', 'asc')->paginate(12);
            return view('home', compact('shops'));
        }
        else{
        	$shop = DB::table('shops')->where('id', '=', auth()->user()->shop_id)->first();
            $products = DB::table('products')->where('shop_id', '=', auth()->user()->shop_id)->paginate(20);
            return view('/shop/products', compact('products', 'shop'));
        }
    }

    public function addproduct()
    {
        $data = request()->validate([
            'name' => ['required', 'string', 'max:191'],
            'cost_price' => ['required', 'numeric'],
            'price' => ['required', 'numeric'],
            'quantity' => ['required', 'numeric']
        ]);

        $tit = str_replace("'","’",$data['name']);
        $tit = str_replace("\"","“",$tit);

        $id = auth()->user()->products()->create([
            'name' => $tit,
            'cost_price' => $data['cost_price'],
            'selling_price' => $data['price'],
            'shop_id' => auth()->user()->shop_id,
            'quantity' => $data['quantity']
        ])->id;

        return redirect()->back()->with("success","Added successfully");
    }

    public function editproduct()
    {
        $data = request()->validate([
        	'id' => ['required', 'numeric'],
            'name' => ['required', 'string', 'max:191'],
            'cost_price' => ['required', 'numeric'],
            'price' => ['required', 'numeric', 'min:0'],
            'quantity' => ['required', 'numeric', 'min:0']
        ]);

        $tit = str_replace("'","’",$data['name']);
        $tit = str_replace("\"","“",$tit);

        $entry = Products::findOrFail($data['id']);
        $entry->name = $tit;
        $entry->cost_price = $data['cost_price'];
        $entry->selling_price = $data['price'];
        $entry->quantity = $data['quantity'];

        $entry->save(); 

        return redirect()->back()->with("success","Edited successfully");
    }

    public function deleteproduct()
    {
        $data = request()->validate([
            'id' => ['required', 'numeric'],
        ]);
        
        DB::table('products')->where('id', '=', $data['id'])->delete();

        return redirect()->back()->with("success","Deleted successfully");
    }
}
