<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Shop;
use DB;
use Hash;
use App\User;

class ShopController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }

    public function addshop()
    {
        $data = request()->validate([
            'name' => ['required', 'string', 'max:191'],
            'location' => ['required', 'string', 'max:191'],
            'contact' => ['required', 'string', 'max:191'],
            'email' => ['required', 'string', 'max:191', 'email', 'unique:users'],
            'adminpassword' => ['required', 'string', 'max:191'],
            'description' => ''
        ]);

        $tit = str_replace("'","’",$data['name']);
        $tit = str_replace("\"","“",$tit);
        $cou = str_replace("'","’",$data['location']);
        $cou = str_replace("\"","“",$cou);
        $cou2 = str_replace("'","’",$data['contact']);
        $cou2 = str_replace("\"","“",$cou2);
        $cou3 = str_replace("'","’",$data['description']);
        $cou3 = str_replace("\"","“",$cou3);

        $id = auth()->user()->shops()->create([
            'name' => $tit,
            'location' => $cou,
            'contact' => $cou2,
            'description' => $cou3
        ])->id;

        $user = new User;
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->shop_id = $id;
        $user->role = 'shopadmin';
        $user->password = Hash::make($data['adminpassword']);
        $user->save();

        return redirect()->back()->with("success","Added successfully");
    }

    public function addattendant()
    {
        $data = request()->validate([
            'name' => ['required', 'string', 'max:191'],
            'phone' => ['required', 'string', 'max:191'],
            'email' => ['required', 'string', 'max:191', 'email', 'unique:users'],
            'password' => ['required', 'string', 'max:191']
        ]);

        $user = new User;
        $user->name = $data['name'];
        $user->email = $data['email'];
        $user->phone = $data['phone'];
        $user->shop_id = auth()->user()->shop_id;
        $user->role = 'attendant';
        $user->password = Hash::make($data['password']);
        $user->save();

        return redirect()->back()->with("success","Added successfully");
    }

    public function editshop()
    {
        $data = request()->validate([
        	'id' => ['required', 'numeric'],
            'name' => ['required', 'string', 'max:191'],
            'location' => ['required', 'string', 'max:191'],
            'contact' => ['required', 'string', 'max:191'],
            'description' => ''
        ]);

        $tit = str_replace("'","’",$data['name']);
        $tit = str_replace("\"","“",$tit);
        $cou = str_replace("'","’",$data['location']);
        $cou = str_replace("\"","“",$cou);
        $cou2 = str_replace("'","’",$data['contact']);
        $cou2 = str_replace("\"","“",$cou2);
        $cou3 = str_replace("'","’",$data['description']);
        $cou3 = str_replace("\"","“",$cou3);

        $entry = Shop::findOrFail($data['id']);
        $entry->name = $tit;
        $entry->location = $cou;
        $entry->contact = $cou2;
        $entry->description = $cou3;

        $entry->save(); 

        return redirect()->back()->with("success","Edited successfully");
    }

    public function deleteshop()
    {
        $data = request()->validate([
            'id' => ['required', 'numeric'],
        ]);
        
        DB::table('shops')->where('id', '=', $data['id'])->delete();
        DB::table('users')->where('shop_id', '=', $data['id'])->delete();

        return redirect()->back()->with("success","Deleted successfully");
    }

    public function deleteattendant()
    {
        $data = request()->validate([
            'id' => ['required', 'numeric'],
        ]);
        
        DB::table('users')->where('id', '=', $data['id'])->delete();

        return redirect()->back()->with("success","Deleted successfully");
    }
    
}
