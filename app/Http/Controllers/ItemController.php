<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Item;

class ItemController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function expenses()
    {
        if (auth()->user()->role == 'admin') {
            $shops = DB::table('shops')->orderBy('name', 'asc')->paginate(12);
            return view('home', compact('shops'));
        }
        else{
            $expenses = DB::table('items')->where('type', '=', 'expense')->paginate(15);
            $shop = DB::table('shops')->where('id', '=', auth()->user()->shop_id)->first();
            return view('/shop/expenses', compact('expenses', 'shop'));
        }
    }

    public function revenues()
    {
        if (auth()->user()->role == 'admin') {
            $shops = DB::table('shops')->orderBy('name', 'asc')->paginate(12);
            return view('home', compact('shops'));
        }
        else{
            $revenues = DB::table('items')->where('type', '=', 'revenue')->paginate(15);
            $shop = DB::table('shops')->where('id', '=', auth()->user()->shop_id)->first();
            return view('/shop/revenues', compact('revenues', 'shop'));
        }
    }

    public function addexpense(){
        $data = request()->validate([
            'name' => ['required', 'string', 'max:191'],
            'description' => ['required', 'string'],
            'nature' => ['required', 'string'],
            'amount' => ['required', 'numeric']
        ]);

        $tit = str_replace("'","’",$data['name']);
        $tit = str_replace("\"","“",$tit);

        $tit2 = str_replace("'","’",$data['description']);
        $tit2 = str_replace("\"","“",$tit2);

        $id = auth()->user()->items()->create([
            'name' => $tit,
            'description' => $tit2,
            'nature' => $data['nature'],
            'type' => 'expense',
            'shop_id' => auth()->user()->shop_id,
            'amount' => $data['amount']
        ])->id;

        return redirect()->back()->with("success","Added successfully");
    }

    public function addrevenue(){
        $data = request()->validate([
            'name' => ['required', 'string', 'max:191'],
            'description' => ['required', 'string'],
            'nature' => ['required', 'string'],
            'amount' => ['required', 'numeric']
        ]);

        $tit = str_replace("'","’",$data['name']);
        $tit = str_replace("\"","“",$tit);

        $tit2 = str_replace("'","’",$data['description']);
        $tit2 = str_replace("\"","“",$tit2);

        $id = auth()->user()->items()->create([
            'name' => $tit,
            'description' => $tit2,
            'nature' => $data['nature'],
            'type' => 'revenue',
            'shop_id' => auth()->user()->shop_id,
            'amount' => $data['amount']
        ])->id;

        return redirect()->back()->with("success","Added successfully");
    }

    public function edititem()
    {
        $data = request()->validate([
            'id' => ['required', 'numeric'],
            'name' => ['required', 'string', 'max:191'],
            'description' => ['required', 'string'],
            'nature' => ['required', 'string'],
            'amount' => ['required', 'numeric']
        ]);

        $tit = str_replace("'","’",$data['name']);
        $tit = str_replace("\"","“",$tit);

        $tit2 = str_replace("'","’",$data['description']);
        $tit2 = str_replace("\"","“",$tit2);

        $entry = Item::findOrFail($data['id']);
        $entry->name = $tit;
        $entry->description = $tit2;
        $entry->nature = $data['nature'];
        $entry->amount = $data['amount'];
        $entry->save(); 

        return redirect()->back()->with("success","Edited successfully");
    }

    public function deleteitem()
    {
        $data = request()->validate([
            'id' => ['required', 'numeric'],
        ]);
        
        DB::table('items')->where('id', '=', $data['id'])->delete();

        return redirect()->back()->with("success","Deleted successfully");
    }
}
