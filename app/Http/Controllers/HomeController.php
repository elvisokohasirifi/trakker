<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\User;
use Auth;
use Hash;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (auth()->user()->role == 'admin') {
            $shops = DB::table('shops')->orderBy('name', 'asc')->paginate(12);
            return view('home', compact('shops'));
        }
        else{
            $user = DB::table('users')->where('shop_id', '=', auth()->user()->shop_id)->count();
            $products = DB::table('products')->where('shop_id', '=', auth()->user()->shop_id)->count();
            $orders = DB::table('orders')->where('shop_id', '=', auth()->user()->shop_id)->count();
            $expenses = DB::table('items')->where('shop_id', '=', auth()->user()->shop_id)->where('type', '=', 'expense')->sum('amount');
            $revenues = DB::table('items')->where('shop_id', '=', auth()->user()->shop_id)->where('type', '=', 'revenue')->sum('amount');
            $shop = DB::table('shops')->where('id', '=', auth()->user()->shop_id)->first();
            $costofsales = DB::table('orders')->select(DB::raw('sum(cost * quantity) as total'))->join('orderings', 'orderings.orders_id', '=', 'orders.id')->where('shop_id', '=', auth()->user()->shop_id)->pluck('total');
            $sales = DB::table('orders')->select(DB::raw('sum(price * quantity) as total'))->join('orderings', 'orderings.orders_id', '=', 'orders.id')->where('shop_id', '=', auth()->user()->shop_id)->pluck('total');
            $allexpenses = DB::table('items')->select(DB::raw('sum(amount) as total'), DB::raw('month(created_at) as month'))->where('shop_id', '=', auth()->user()->shop_id)->where('type', '=', 'expense')->groupBy('month')->get();
            $allrevenues = DB::table('items')->select(DB::raw('sum(amount) as total'), DB::raw('month(created_at) as month'))->where('shop_id', '=', auth()->user()->shop_id)->where('type', '=', 'revenue')->groupBy('month')->get();
            $allsales = DB::table('orders')->select(DB::raw('sum(price * quantity) as total'), DB::raw('month(orders.created_at) as month'))->join('orderings', 'orderings.orders_id', '=', 'orders.id')->where('shop_id', '=', auth()->user()->shop_id)->groupBy('month')->get();
            return view('/shop/home', compact('user', 'products', 'orders', 'shop', 'expenses', 'revenues', 'costofsales', 'sales', 'allexpenses', 'allsales', 'allrevenues'));
        }
    }

    public function shophome()
    {
        if (auth()->user()->role == 'admin') {
            $shops = DB::table('shops')->orderBy('name', 'asc')->paginate(12);
            return view('home', compact('shops'));
        }        
        else{
            $user = DB::table('users')->where('shop_id', '=', auth()->user()->shop_id)->count();
            $products = DB::table('products')->where('shop_id', '=', auth()->user()->shop_id)->count();
            $orders = DB::table('orders')->where('shop_id', '=', auth()->user()->shop_id)->count();
            $expenses = DB::table('items')->where('shop_id', '=', auth()->user()->shop_id)->where('type', '=', 'expense')->sum('amount');
            $revenues = DB::table('items')->where('shop_id', '=', auth()->user()->shop_id)->where('type', '=', 'revenue')->sum('amount');
            $shop = DB::table('shops')->where('id', '=', auth()->user()->shop_id)->first();
            $costofsales = DB::table('orders')->select(DB::raw('sum(cost * quantity) as total'))->join('orderings', 'orderings.orders_id', '=', 'orders.id')->where('shop_id', '=', auth()->user()->shop_id)->pluck('total');
            $sales = DB::table('orders')->select(DB::raw('sum(price * quantity) as total'))->join('orderings', 'orderings.orders_id', '=', 'orders.id')->where('shop_id', '=', auth()->user()->shop_id)->pluck('total');
            $allexpenses = DB::table('items')->select(DB::raw('sum(amount) as total'), DB::raw('month(created_at) as month'))->where('shop_id', '=', auth()->user()->shop_id)->where('type', '=', 'expense')->groupBy('month')->get();
            $allrevenues = DB::table('items')->select(DB::raw('sum(amount) as total'), DB::raw('month(created_at) as month'))->where('shop_id', '=', auth()->user()->shop_id)->where('type', '=', 'revenue')->groupBy('month')->get();
            $allsales = DB::table('orders')->select(DB::raw('sum(price * quantity) as total'), DB::raw('month(orders.created_at) as month'))->join('orderings', 'orderings.orders_id', '=', 'orders.id')->where('shop_id', '=', auth()->user()->shop_id)->groupBy('month')->get();
            return view('/shop/home', compact('user', 'products', 'orders', 'shop', 'expenses', 'revenues', 'costofsales', 'sales', 'allexpenses', 'allsales', 'allrevenues'));
        }
    }

    public function search(){
        $data = request()->validate([
            'search' => ['required', 'string', 'max:191'],
        ]);
        $users = DB::table('users')->where('name', 'like', '%'.$data['search'].'%')->orWhere('sid', 'like', '%'.$data['search'].'%')->orWhere('phonenumber', 'like', '%'.$data['search'].'%')->latest()->paginate(10);

        return view('users', compact('users'));
    }


    public function edituserstatus(){
        $data = request()->validate([
            'id' => ['required', 'numeric'],
            'userstatus' => ['required', 'string']
        ]);
        $entry = User::findOrFail($data['id']);
        $entry->status = $data['userstatus'];
        $entry->save();
        return redirect()->back()->with("success","Status updated successfully");
    }

    public function sendnotif(){
        //dd(request()->all());
        $data = request()->validate([
            'title' => ['required', 'string', 'max:191'],
            'type' => ['required', 'string', 'max:7'],
            'message' => ['required'],
            'name' => ['required', 'string', 'max:191'],
            'deviceid' => '',
            'email' => ['required', 'string', 'max:191'],
            'id' => ''
        ]);

        auth()->user()->allnotifications()->create([
            'subject' => $data['title'],
            'type' => $data['type'],
            'message' => $data['message'],
            'recipient' => $data['id']
        ]);

        //dd($data);

        $feedback = "";

        if ($data['type'] == 'email') {
            $feedback = sendMyMail($data['email'], $data['title'], $data['message']);
        }

        else{
            $feedback = sendSingleNotification($data['title'], $data['message'], $data['deviceid']);
        }

        return redirect()->back()->with("success", $feedback);
    }

    function loadaccountuser(){
        $user = User::findOrFail(auth()->user()->id);
        return view('account', compact('user'));
    }

    function changepassword(){
        $user = User::findOrFail(auth()->user()->id);
        return view('password', compact('user'));
    }

    public function update(){
        $data = request()->validate([
            'curpass' => ['required', 'string'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
        if (!(Hash::check($data['curpass'], Auth::User()->password))) {
            // passwords matches
            return redirect()->back()->with("error","Your current password does not match with the password you provided");
        }

        if(strcmp($data['curpass'], $data['password']) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","New password cannot be same as your current password");
        }
        if(Auth::Check()){
            $current_password = Auth::User()->password;           
            if(Hash::check($data['curpass'], $current_password)){         
                $user_id = Auth::User()->id;                       
                $obj_user = User::find($user_id);
                $obj_user->password = Hash::make($data['password']);;
                $obj_user->save(); 
                return redirect()->back()->with("success","Password changed successfully!");
            }
            else
                return redirect()->back()->with("error","Password could not be changed");
        }
        else
            return redirect()->back()->with("error","Password could not be changed");
    }

    public function edit(){
        $user_id = Auth::User()->id;                       
        $obj_user = User::find($user_id);
        if (strcasecmp($obj_user->email, request('email')) != 0) {
            //$obj_user->email = $data['email'];
        
            $data = request()->validate([
                'name' => ['required', 'max:255'],
                'email' => ['required', 'email', 'max:255', 'unique:users'],
                'phonenumber' => '',
            ]);

            $obj_user->name = $data['name'];
            $obj_user->email = $data['email'];
            $obj_user->phone = $data['phonenumber'];
            $obj_user->save();
            return redirect()->back()->with("success","Account information updated successfully!");
        }
        else{
            $data = request()->validate([
                'name' => ['required', 'max:255'],
                'email' => '',
                'phonenumber' => ''
            ]);
            $obj_user->name = $data['name'];
            $obj_user->phone = $data['phonenumber'];
            $obj_user->save();
            return redirect()->back()->with("success","Account information updated successfully!");
        }
    }

    public function users(){
        $shop = DB::table('shops')->where('id', '=', auth()->user()->shop_id)->first();
        $users = DB::table('users')->where('shop_id', '=', auth()->user()->shop_id)->orderBy('name', 'asc')->paginate(10);
        if (auth()->user()->role == 'admin') {
            $shops = DB::table('shops')->orderBy('name', 'asc')->paginate(12);
            return view('home', compact('shops'));
        }
        else if(auth()->user()->role != 'shopadmin'){
            $user = DB::table('users')->where('shop_id', '=', auth()->user()->shop_id)->count();
            $products = DB::table('products')->where('shop_id', '=', auth()->user()->shop_id)->count();
            $orders = DB::table('orders')->where('shop_id', '=', auth()->user()->shop_id)->count();
            $payments = DB::table('payments')->where('shop_id', '=', auth()->user()->shop_id)->count();
            $shop = DB::table('shops')->where('id', '=', auth()->user()->shop_id)->first();
            return view('/shop/home', compact('user', 'products', 'orders', 'payments', 'shop'));
        }
        return view('/shop/users', compact('users', 'shop'));
    }
}
