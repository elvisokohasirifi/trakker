<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payments extends Model
{
    protected $fillable = [
        'payment_method', 'amount', 'payment_no', 'status'
    ];

    protected $guarded = [];

    public function order(){
        return $this->belongsTo(Orders::class);
    }
}
