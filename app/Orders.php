<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    protected $fillable = [
        'order_no', 'payment_status', 'order_status', 'deviceid'
    ];

    protected $guarded = [];

    public function orderings(){
        return $this->hasMany(Ordering::class)->orderBy('created_at', 'DESC');
    }

    public function payment(){
        return $this->hasMany(Payments::class)->orderBy('created_at', 'DESC');
    }

}
