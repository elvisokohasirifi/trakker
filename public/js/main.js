var CRUMINA={};!function(e){"use strict";var n=e(window),t=e(document),o=e("body"),i=e(".fixed-sidebar"),a=e("#hellopreloader");CRUMINA.preloader=function(){return n.scrollTop(0),setTimeout(function(){a.fadeOut(800)},500),!1},jQuery(".back-to-top").on("click",function(){return e("html,body").animate({scrollTop:0},1200),!1}),e(document).on("click",".quantity-plus",function(){var n=parseInt(e(this).prev("input").val());return e(this).prev("input").val(n+1).change(),!1}),e(document).on("click",".quantity-minus",function(){var n=parseInt(e(this).next("input").val());return 1!==n&&e(this).next("input").val(n-1).change(),!1}),e(function(){var n;e(document).on("touchstart mousedown",".number-spinner button",function(){var t=e(this),o=t.closest(".number-spinner").find("input");t.closest(".number-spinner").find("button").prop("disabled",!1),n="up"==t.attr("data-dir")?setInterval(function(){void 0==o.attr("max")||parseInt(o.val())<parseInt(o.attr("max"))?o.val(parseInt(o.val())+1):(t.prop("disabled",!0),clearInterval(n))},50):setInterval(function(){void 0==o.attr("min")||parseInt(o.val())>parseInt(o.attr("min"))?o.val(parseInt(o.val())-1):(t.prop("disabled",!0),clearInterval(n))},50)}),e(document).on("touchend mouseup",".number-spinner button",function(){clearInterval(n)})}),e('a[data-toggle="tab"]').on("shown.bs.tab",function(n){"#events"===e(n.target).attr("href")&&e(".fc-state-active").click()}),e(".js-sidebar-open").on("click",function(){return e("body").outerWidth()<=560&&e(this).closest("body").find(".popup-chat-responsive").removeClass("open-chat"),e(this).toggleClass("active"),e(this).closest(i).toggleClass("open"),!1}),n.keydown(function(e){27==e.which&&i.is(":visible")&&i.removeClass("open")}),t.on("click",function(n){!e(n.target).closest(i).length&&i.is(":visible")&&i.removeClass("open")});var r=e(".window-popup");e(".js-open-popup").on("click",function(n){var t=e(this).data("popup-target"),i=r.filter(t),a=e(this).offset();return i.addClass("open"),i.css("top",a.top-i.innerHeight()/2),o.addClass("overlay-enable"),!1}),n.keydown(function(n){27==n.which&&(r.removeClass("open"),o.removeClass("overlay-enable"),e(".profile-menu").removeClass("expanded-menu"),e(".popup-chat-responsive").removeClass("open-chat"),e(".profile-settings-responsive").removeClass("open"),e(".header-menu").removeClass("open"),e(".js-sidebar-open").removeClass("active"))}),t.on("click",function(n){e(n.target).closest(r).length||(r.removeClass("open"),o.removeClass("overlay-enable"),e(".profile-menu").removeClass("expanded-menu"),e(".header-menu").removeClass("open"),e(".profile-settings-responsive").removeClass("open"))}),e("[data-toggle=tab]").on("click",function(){if(e(this).hasClass("active")&&e(this).closest("ul").hasClass("mobile-app-tabs"))return e(e(this).attr("href")).toggleClass("active"),e(this).removeClass("active"),!1}),e(".js-close-popup").on("click",function(){return e(this).closest(r).removeClass("open"),o.removeClass("overlay-enable"),!1}),e(".profile-settings-open").on("click",function(){return e(".profile-settings-responsive").toggleClass("open"),!1}),e(".js-expanded-menu").on("click",function(){return e(".header-menu").toggleClass("expanded-menu"),!1}),e(".js-chat-open").on("click",function(){return e(".popup-chat-responsive").toggleClass("open-chat"),!1}),e(".js-chat-close").on("click",function(){return e(".popup-chat-responsive").removeClass("open-chat"),!1}),e(".js-open-responsive-menu").on("click",function(){return e(".header-menu").toggleClass("open"),!1}),e(".js-close-responsive-menu").on("click",function(){return e(".header-menu").removeClass("open"),!1}),CRUMINA.CallToActionAnimation=function(){var e=new ScrollMagic.Controller;new ScrollMagic.Scene({triggerElement:".call-to-action-animation"}).setVelocity(".first-img",{opacity:1,bottom:"0",scale:"1"},1200).triggerHook(1).addTo(e),new ScrollMagic.Scene({triggerElement:".call-to-action-animation"}).setVelocity(".second-img",{opacity:1,bottom:"50%",right:"40%"},1500).triggerHook(1).addTo(e)},CRUMINA.ImgScaleAnimation=function(){var e=new ScrollMagic.Controller;new ScrollMagic.Scene({triggerElement:".img-scale-animation"}).setVelocity(".main-img",{opacity:1,scale:"1"},200).triggerHook(.3).addTo(e),new ScrollMagic.Scene({triggerElement:".img-scale-animation"}).setVelocity(".first-img1",{opacity:1,scale:"1"},1200).triggerHook(.8).addTo(e),new ScrollMagic.Scene({triggerElement:".img-scale-animation"}).setVelocity(".second-img1",{opacity:1,scale:"1"},1200).triggerHook(1.1).addTo(e),new ScrollMagic.Scene({triggerElement:".img-scale-animation"}).setVelocity(".third-img1",{opacity:1,scale:"1"},1200).triggerHook(1.4).addTo(e)},CRUMINA.SubscribeAnimation=function(){var e=new ScrollMagic.Controller;new ScrollMagic.Scene({triggerElement:".subscribe-animation"}).setVelocity(".plane",{opacity:1,bottom:"auto",top:"-20",left:"50%",scale:"1"},1200).triggerHook(1).addTo(e)},CRUMINA.PlanerAnimation=function(){var e=new ScrollMagic.Controller;new ScrollMagic.Scene({triggerElement:".planer-animation"}).setVelocity(".planer",{opacity:1,left:"80%",scale:"1"},2e3).triggerHook(.1).addTo(e)},CRUMINA.ContactAnimationAnimation=function(){var e=new ScrollMagic.Controller;new ScrollMagic.Scene({triggerElement:".contact-form-animation"}).setVelocity(".crew",{opacity:1,left:"77%",scale:"1"},1e3).triggerHook(.1).addTo(e)},CRUMINA.perfectScrollbarInit=function(){var n=e(".popup-chat-responsive .mCustomScrollbar");e(".mCustomScrollbar").perfectScrollbar({wheelPropagation:!1}),n.length&&(n.scrollTop(n.prop("scrollHeight")),n.perfectScrollbar("update"))},CRUMINA.responsive={$profilePanel:null,$desktopContainerPanel:null,$responsiveContainerPanel:null,init:function(){this.$profilePanel=jQuery("#profile-panel"),this.$desktopContainerPanel=jQuery("#desktop-container-panel > .ui-block"),this.$responsiveContainerPanel=jQuery("#responsive-container-panel .ui-block"),this.update()},mixPanel:function(){window.matchMedia("(max-width: 1024px)").matches?this.$responsiveContainerPanel.append(this.$profilePanel):this.$desktopContainerPanel.append(this.$profilePanel)},update:function(){var n=this,t=null,o=function(){t=null,n.mixPanel()};e(window).on("resize",function(){null===t&&(t=window.setTimeout(function(){o()},300))}).resize()}},t.ready(function(){CRUMINA.preloader(),CRUMINA.perfectScrollbarInit(),e(".call-to-action-animation").length&&CRUMINA.CallToActionAnimation(),e(".img-scale-animation").length&&CRUMINA.ImgScaleAnimation(),e(".subscribe-animation").length&&CRUMINA.SubscribeAnimation(),e(".planer-animation").length&&CRUMINA.PlanerAnimation(),e(".contact-form-animation").length&&CRUMINA.ContactAnimationAnimation(),void 0!==e.fn.gifplayer&&e(".gif-play-image").gifplayer(),void 0!==e.fn.mediaelementplayer&&e("#mediaplayer").mediaelementplayer({features:["prevtrack","playpause","nexttrack","loop","shuffle","current","progress","duration","volume"]}),CRUMINA.responsive.init()})}(jQuery);

function editshop(id, name, location, contact, description){
	document.getElementById('ename').value = name;
    document.getElementById('elocation').value = location;
    document.getElementById('econtact').value = contact;
    document.getElementById('eid').value = id;
    document.getElementById('edescription').value = description;
}

function deleteshop(id){
    document.getElementById('did').value = id;
}

function editproduct(id, name, cost_price, selling_price, qty){
	  document.getElementById('ename').value = name;
    document.getElementById('ecost').value = cost_price;
    document.getElementById('eprice').value = selling_price;
    document.getElementById('equantity').value = qty;
    document.getElementById('eid').value = id;
}

function view(id, orderno){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.post("/vieworder", {id: id},
        function(data, status){
          let ans = data['message'];
          let total = 0;
          let tab = "<table class='table table-striped'><tr><th>Product</th><th>Price</th><th>Quantity</th><th>Total</th></tr>";
          for(var i = 0; i < ans.length; i++){
              tab += '<tr>';
              tab += ('<td>' + ans[i]['product'] + "</td>");
              tab += ('<td>' + ans[i]['price'] + "</td>");
              tab += ('<td>' + ans[i]['quantity'] + "</td>");
              tab += ('<td>' + ans[i]['quantity'] * ans[i]['price'] + "</td>");
              total += (ans[i]['quantity'] * ans[i]['price']);
              tab += '</tr>';
          }
          tab += "<tr><td><b>Total</b></td><td></td><td></td><td><b>GHS " + total + "</b></td></tr></table>";
          $('#viewmodal').modal('show'); 
        document.getElementById('orderbox').innerHTML = tab;
        document.getElementById('orderno').innerHTML = orderno;          
    });
    
    //document.getElementById('id').innerHTML = id;
 }

 function getorders(id){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    let tab = "<table class='table table-striped'><tr><th>Product</th><th>Price</th><th>Quantity</th><th>Total</th></tr>";

    $.post("/vieworder", {id: id},
        function(data, status){
          let ans = data['message'];
          for(var i = 0; i < ans.length; i++){
              tab += '<tr>';
              tab += ('<td>' + ans[i]['product'] + "</td>");
              tab += ('<td>' + ans[i]['price'] + "</td>");
              tab += ('<td>' + ans[i]['quantity'] + "</td>");
              tab += ('<td>' + ans[i]['quantity'] * ans[i]['price'] + "</td>");
              tab += '</tr>';
          }
          tab += "</table>";
          return tab;
    });
    console.log(tab);
    
 }

 function printreceipt(id){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.post("/vieworder", {id: id},
        function(data, status){
          let ans = data['message'];
          let tab = "<table class='table table-striped'><tr><th>Product</th><th>Price</th><th>Quantity</th><th>Total</th></tr>";
          for(var i = 0; i < ans.length; i++){
              tab += '<tr>';
              tab += ('<td>' + ans[i]['product'] + "</td>");
              tab += ('<td>' + ans[i]['price'] + "</td>");
              tab += ('<td>' + ans[i]['quantity'] + "</td>");
              tab += ('<td>' + ans[i]['quantity'] * ans[i]['price'] + "</td>");
              tab += '</tr>';
          }
          tab += "</table>";
          document.getElementById('orderbox').innerHTML = tab;
          document.getElementById('printer').onclick = function() {
            printJS('orderbox', 'html');
          }
          document.getElementById('printer').click();
          updateOrder(id);
        });
 }

function updateOrder(id){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $.post("/updateorder", {id: id},
    function(data, status){
      document.getElementById('order' + data['message']).innerHTML = "completed";
    });
}

function edititem(id, name, description, nature, amount){
    document.getElementById('ename').value = name;
    document.getElementById('edescription').value = description;
    document.getElementById('enature').value = nature;
    document.getElementById('eamount').value = amount;
    document.getElementById('eid').value = id;
}


let cartitems = new Array();

function addtocart(id, name, price, remaining){
  if (cartitems.findIndex(x => x.id === id) > -1) {
    //alert('item already added');
    var position = cartitems.findIndex(x => x.id === id);
    if (cartitems[position]['qty'] >= remaining) {
      //alert('You do not have any more of this product in stock');
      Swal.fire({
        title: 'Sorry!',
        text: 'You do not have any more of this product in stock',
        icon: 'error'
      });
      return;
    }
    cartitems[position]['qty'] += 1;
    document.getElementById('cartitems').innerHTML = updatecartview();
    document.getElementById('ordertotal').innerHTML = getTotal();
    return;
  }
  else{
    cartitems.push({'id': id, 'name': name, 'price': price, 'qty': 1});
    document.getElementById('cartitems').innerHTML = updatecartview();
    document.getElementById('ordertotal').innerHTML = getTotal();
  }
}

function getTotal(){
  if (cartitems == null || cartitems.length == 0) {
    return 0;
  }
  return cartitems.reduce((i, j) => i + j.price * j.qty, 0);
}

function updatecartview(){
  var s = "";
  for (var i = cartitems.length - 1; i >= 0; i--) {
    s += 
          '<tr class="cart_item">\
    \
            <td class="product-thumbnail">\
    \
                <div class="cart-product__item">\
                    <div class="cart-product-content">\
                        <a href="#" class="product-category">' + cartitems[i]['name'] + '</a>\
                    </div>\
                </div>\
            </td>\
    \
            <td class="product-price">\
                <h6 class="price amount">' + cartitems[i]['price'] + '</h6>\
            </td>\
    \
            <td class="product-quantity">\
                <input type="number" name="qty" value="' + cartitems[i]['qty'] + '" style="width: 50px; padding: 3px" onchange="updatetotals(' + cartitems[i]['id'] + ', this.value)" onkeyup="updatetotals(' + cartitems[i]['id'] + ', this.value)">' + 

            '</td>\
    \
            <td class="product-subtotal">\
                <h6 class="total amount">' + cartitems[i]['price'] * cartitems[i]['qty']  + '</h6>\
            </td>\
    \
            <td class="product-remove" style="text-align: right">\
                <button class="btn btn-sm bg-orange" onclick="removefromcart(' + cartitems[i]['id'] + ')">x</button>\
            </td>\
        </tr>';
  }
  return s;
}

function updatetotals(id, val){
  if (!isNaN(val)) {
    var position = cartitems.findIndex(x => x.id === id);
    cartitems[position]['qty'] = val;
    document.getElementById('cartitems').innerHTML = updatecartview();
    document.getElementById('ordertotal').innerHTML = getTotal();
  }
}

function removefromcart(id){
  var position = cartitems.findIndex(x => x.id === id);
  if(position > -1){
    cartitems.splice(position, 1);
    document.getElementById('cartitems').innerHTML = updatecartview();
    document.getElementById('ordertotal').innerHTML = getTotal();
  }
}

function completeorder(){
  $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    document.getElementById('container').style.opacity = 0.5;
    var d = new Date();
    var payment = document.querySelector('input[name="optionsRadios"]:checked').value;
    $.post("/shop/completeorder", {cart: cartitems, order_no: d.getTime(), payment: payment},
      function(data, status){
        if (data['success'] == 1) {
          Swal.fire({
            title: 'success!',
            text: 'Completed',
            icon: 'success'
          });
          window.location.href = '/shop/sales';
        }
        else{
          Swal.fire({
            title: 'Sorry!',
            text: 'Your sale was not completed. Please try again',
            icon: 'error'
          });
          //alert('Your sale was not completed. Please try again');
        }
        document.getElementById('container').style.opacity = 1;
      }
    );
    //console.log(tab);
}

const API_publicKey = "FLWPUBK_TEST-55989376ec82d8fde1b8280325632463-X";

function payWithRave(amount, name, desc, nature) {
    var dates = Date.now();
    var x = getpaidSetup({
        PBFPubKey: API_publicKey,
        customer_email: document.getElementById('email').value,
        amount: amount,
        customer_phone: document.getElementById('phone').value,
        customer_firstname: document.getElementById('name').value,
        currency: "NGN",
        txref: dates,
        meta: [{
            metaname: "flightID",
            metavalue: "AP1234"
        }],
        onclose: function() {},
        callback: function(response) {
            var txref = response.tx.txRef; // collect txRef returned and pass to a                  server page to complete status check.
            //console.log("This is the response returned after a charge", response);
            if (
                response.tx.chargeResponseCode == "00" ||
                response.tx.chargeResponseCode == "0"
            ) {
                //alert('success');
                success(dates, amount, name, desc, nature, response.tx.paymentType);
            } else {
                Swal.fire({
                  icon: 'error',
                  title: 'Oops...',
                  text: response.data.responsemessage
                });
            }

            x.close(); // use this to close the modal immediately after payment.
        }
    });
}

function success(dates, amount, name, desc, nature, method){
  $.ajaxSetup({           
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });
  
  $.post("/shop/completepayment", {payment_no: dates, amount: amount, name: name, description: desc, nature: nature, payment_method: method},
    function(data, status){
      if (data['success'] == 1) {
        Swal.fire({
          icon: 'success',
          title: 'Yayyy...',
          text: 'Your payment has been completed'
        });
        window.location.href = '/shop/expenses';
      }
      else{
        console.log(data);
        Swal.fire({
          icon: 'error',
          title: 'Awww...',
          text: 'Your payment could not be completed'
        });
      }
    });
}

$("#submitexpenseform").click(function(e){
  e.preventDefault();
  let checked = document.getElementById('send').checked;
    if (!checked) {
      $("#addexpenseform").submit();
    }
    else{
      let name = document.getElementById('username').value;
      let description = document.getElementById('description').value;
      let nature = document.getElementById('nature').value;
      let amount = document.getElementById('amount').value;
      payWithRave(amount, name, description, nature);
    }
});

