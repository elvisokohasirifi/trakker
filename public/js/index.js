function validateEmail(email) {
    var re = /\S+@\S+\.\S+/;
    return re.test(email);
}

function login(email, password){
	$.post("../php/functions.php",
	{
		login : 1, 
		email : email, 
		password : password
	},
	function(data, status){
		if(data['success'] == 1){
			window.location.href = "home.php";
		}
		else{
			alert(data["message"]);
		}
	});
}

function signup(fname, lname, email, phone, gender, dob, role, password){
	//alert(fname + lname + email + phone + gender + dob + role + password);
	$.post("../php/functions.php",
	{
		signup: 1, 
		fname: fname, 
		lname: lname, 
		email: email, 
		phone: phone, 
		gender: gender, 
		dob: dob, 
		role: role, 
		password: password
	},
	function(data, status){
		alert(data['message']);
	});
}

function registerme(){
	$("#register").css({ opacity: 0.5 });
    var fname = document.getElementById('fname').value;
    var lname = document.getElementById('lname').value;
    var email = document.getElementById('email').value;
    var phone = document.getElementById('phone').value;
    var gender = document.getElementById('gender').value;
    var dob = document.getElementById('dob').value;
    var password = document.getElementById('password').value;
	var role = 'Student';
	if(validateEmail(email) && password.length > 6 && phone.length > 7 ){
		signup(fname, lname, email, phone, gender, dob, role, password);
	}
	else if(phone.length <= 7){
		alert("Please enter a valid phone number");
	}
	else if(!validateEmail(email)){
		alert("Please enter a valid email address");
	}
	else{
		alert("Password must be more than 6 characters");
	}
	$("#register").css({ opacity: 1 });
}

function logmein(){
    var email = document.getElementById('logemail').value;
	var password = document.getElementById('logpass').value;
	if(validateEmail(email) && password.length > 6){
		login(email, password);
	}
	else{
		alert("invalid credentials");
	}
    //window.location.href = "home.php";
}